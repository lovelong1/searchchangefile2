package cn.wangkai.peanut.searchchangefile;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import cn.wangkai.peanut.searchchangefile.from.LabelForm;
import cn.wangkai.peanut.searchchangefile.from.TextForm;
import cn.wangkai.peanut.searchchangefile.swtdesigner.SWTResourceManager;
import cn.wangkai.peanut.searchchangefile.tools.FileHeap;
import cn.wangkai.peanut.searchchangefile.tools.FromUtil;
import cn.wangkai.peanut.searchchangefile.tools.ProUtil;
import cn.wangkai.peanut.searchchangefile.tools.SearchFileException;
import cn.wangkai.peanut.searchchangefile.tools.WKCalendar;

public class CheckFile {

	private Text outm;
	private Text checkdate;
	private Text sourceout;
	private Text sourcein;
	private Button iscopy;
	protected Shell CheckFileshell;

	// gif
	static Display display;
	static GC shellGC;
	static Color shellBackground;
	static Image image;
	static final boolean useGIFBackground = false;

	/**
	 * 启动程序
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new CheckFile().open();
	}

	/**
	 * 显示窗体
	 * 
	 */
	public void open() {
		final Display display = Display.getDefault();
		createContents();
		FromUtil.centerShell(CheckFileshell);
		CheckFileshell.open();
		CheckFileshell.layout();
		while (!CheckFileshell.isDisposed()) {
			if (!display.readAndDispatch()) display.sleep();
		}
	}

	/**
	 * Create contents of the window
	 */
	protected void createContents() {

		CheckFileshell = new Shell(SWT.MIN | SWT.CLOSE);
		CheckFileshell.setImage(SWTResourceManager.getImage(CheckFile.class, "/icons/note.gif"));
		CheckFileshell.setText("变更文件查询工具");
		
		shellBackground = Display.getCurrent().getSystemColor(SWT.COLOR_WHITE);
		
		
		final Composite composite = new Composite(CheckFileshell, SWT.NONE);
		final GridData gridData_2 = new GridData(GridData.HORIZONTAL_ALIGN_FILL);

		gridData_2.heightHint = 125;
		gridData_2.widthHint = 650;
		
		composite.setLayoutData(gridData_2);
		composite.setBackground(shellBackground);

		shellGC = new GC(composite);
		
		final Label MessageText =  new LabelForm(composite, SWT.NONE, 10, 10, 300, 100,shellBackground);
		final Label headbackimg =  new LabelForm(composite, SWT.NONE, 575, 0, 75, 56);
		final Label headplyerimg = new LabelForm(composite, SWT.NONE, 575, 61, 75, 64,shellBackground);

		MessageText.setText("本软件主要查找日期时间之后的变更新的文件。\n该软件为绿色软件。");
		headbackimg.setImage(SWTResourceManager.getImage(CheckFile.class, "/icons/coderefact_wiz.gif"));
		headplyerimg.setImage(SWTResourceManager.getImage(CheckFile.class, "/icons/run_wiz.gif"));
		
		
		
//		final Label label_3 = new Label(CheckFileshell, SWT.SEPARATOR | SWT.HORIZONTAL);
//		label_3.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		
		
		
		
		Group group_1;
		Button button_1;
		
		/**
		 * 选项标签
		 */
		Group configgroup = new Group(CheckFileshell, SWT.NONE);
		final GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_FILL);
//		gridData.heightHint = 100;
//		gridData.widthHint = 800;
		configgroup.setLayoutData(gridData);
		configgroup.setText("选项");
		
		/**
		 * 源文件夹
		 */
		new LabelForm(configgroup, SWT.NONE, 			25, 20, 50,  20, "源文件夹");
		sourcein = new TextForm(configgroup, SWT.BORDER,85, 20, 500, 20);
		Button sourcebtn;
		sourcebtn = new Button(configgroup, SWT.NONE);
		sourcebtn.setBounds(585, 20, 40, 20);
		sourcebtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				DirectoryDialog directoryDialog = new DirectoryDialog(CheckFileshell);
				directoryDialog.setText("请选择文件夹");
				directoryDialog.setMessage("请选择需要源文件夹");
				if (sourcein.getText() != null)
					directoryDialog.setFilterPath(sourcein.getText());

				String filepath = directoryDialog.open();
				if (filepath != null)
					sourcein.setText(filepath);
			}
		});
		sourcebtn.setText("...");
		
		/**
		 * 目标文件夹
		 */
		new LabelForm(configgroup, SWT.NONE,		 		15, 45, 70,  20, "目标文件夹");
		sourceout = new TextForm(configgroup, SWT.BORDER,	85, 45, 500, 20);
		Button targetbtn = new Button(configgroup, SWT.NONE);
		targetbtn.setBounds(585, 45, 40, 20);
		targetbtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				DirectoryDialog directoryDialog = new DirectoryDialog(CheckFileshell);
				directoryDialog.setText("请选择文件夹");
				directoryDialog.setMessage("请选择变动文件的保存路径");
				if (sourceout.getText() != null)
					directoryDialog.setFilterPath(sourceout.getText());
				// CheckFile.ShowMessage(shell, directoryDialog.open());
				String filepath = directoryDialog.open();
				if (filepath != null)
					sourceout.setText(filepath);
			}
		});
		targetbtn.setText("...");
		
		/**
		 * 
		 */
		new LabelForm(configgroup, SWT.NONE, 20, 70, 55, 20, "日期时间");
		checkdate = new TextForm(configgroup, SWT.BORDER,85, 70, 140, 20);

		Button button_3;
		button_3 = new Button(configgroup, SWT.NONE);
		button_3.setBounds(225, 70, 40, 20);
		button_3.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				WKCalendar calendar = new WKCalendar(CheckFileshell);
				Point point = new Point(CheckFileshell.getLocation().x + checkdate.getLocation().x+configgroup.getLocation().x,
						CheckFileshell.getLocation().y + checkdate.getLocation().y + checkdate.getBounds().height +180);
				Object object = calendar.open(point);
				if (object != null) {
					String checkdatestring = String.valueOf(object);
					if (StringUtils.isNotEmpty(checkdatestring)) checkdate.setText(checkdatestring + " 00:00:00");
				}
			}
		});
		button_3.setText("...");

		iscopy = new Button(configgroup, SWT.CHECK);
		iscopy.setBounds(265, 70, 100, 20);
		iscopy.setText("直接拷贝文件");



		button_1 = new Button(configgroup, SWT.NONE);
		button_1.setBounds(365, 70, 85, 20);
		button_1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				outm.setText("sourcein=" + sourcein.getText() + "\n" + "sourceout=" + sourceout.getText() + "\n" + "checkdate=" + checkdate.getText() + "\n" + "iscopy=" + iscopy.getSelection());
				try {
					if (StringUtils.isEmpty(sourcein.getText())) throw new SearchFileException("请选择源文件夹");
					if (StringUtils.isEmpty(sourceout.getText())) throw new SearchFileException("请选择目标文件夹");
					if ("/".equals(sourcein.getText().substring(sourcein.getText().length() - 1))) throw new SearchFileException("文件夹路径最后不要带\"\\\"!");
					if ("/".equals(sourceout.getText().substring(sourceout.getText().length() - 1))) throw new SearchFileException("文件夹路径最后不要带\"\\\"!");
					if (StringUtils.isEmpty(checkdate.getText())) throw new SearchFileException("请选择日期，包含时间");
					
					FileHeap fileHeap = new FileHeap();
					fileHeap.setFilePath(sourcein.getText());
					fileHeap.setFileOut(sourceout.getText());
					fileHeap.setCheckDate(checkdate.getText());
					fileHeap.setCopy(iscopy.getSelection());
					fileHeap.DoIt();
					outm.setText(fileHeap.getOutStr());
					writeconfig();
				} catch (SearchFileException e) {
					FromUtil.ShowMessage(CheckFileshell, e.getMessage());
				} catch (Exception e) {
					FromUtil.ShowMessage(CheckFileshell, "出现错误：" + ExceptionUtils.getStackTrace(e));
					outm.append(ExceptionUtils.getStackTrace(e));
				}

			}
		});
		button_1.setText("开始查询");

		group_1 = new Group(CheckFileshell, SWT.NONE);
		final GridData gridData_1 = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_FILL);
		gridData_1.heightHint = 220;
		//gridData_1.widthHint = 498;
		group_1.setLayoutData(gridData_1);
		group_1.setText("输出");

		outm = new Text(group_1, SWT.V_SCROLL | SWT.BORDER | SWT.WRAP | SWT.H_SCROLL);
		outm.setBounds(10, 15, 630, 200);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 0;
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		gridLayout.horizontalSpacing = 0;
		CheckFileshell.setLayout(gridLayout);
		CheckFileshell.pack();
		//
		readconfig();
	}

	/**
	 * 配置文件获取上次写入配置
	 */
	private void readconfig() {
		sourcein.setText(ProUtil.read("sourcein", "", false));
		sourceout.setText(ProUtil.read("sourceout", "", false));
		checkdate.setText(ProUtil.read("checkdate", "", false));
		iscopy.setSelection((StringUtils.equalsIgnoreCase("0", ProUtil.read("iscopy", "0", false))) ? false : true);
	}
	
	/**
	 * 将当前作业写入配置
	 */
	private void writeconfig() {
		ProUtil.write("sourcein", sourcein.getText());
		ProUtil.write("sourceout", sourceout.getText());
		ProUtil.write("checkdate", checkdate.getText());
		ProUtil.write("iscopy", iscopy.getSelection() ? "1" : "0");
	}
	
}
