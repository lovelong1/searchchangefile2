package cn.wangkai.peanut.searchchangefile.from;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.graphics.Color;

public class LabelForm extends Label {
	public LabelForm(Composite parent, int style,int x, int y, int width, int height,Color color) {
		super(parent, style);
		super.setBounds(x, y, width, height);
		if(color!=null) super.setBackground(color);
	}
	
	public LabelForm(Composite parent, int style,int x, int y, int width, int height,String text) {
		super(parent, style);
		super.setBounds(x, y, width, height);
		if(text!=null) super.setText(text);
	}
	
	public LabelForm(Composite parent, int style,int x, int y, int width, int height,Color color,String text) {
		super(parent, style);
		super.setBounds(x, y, width, height);
		if(color!=null) super.setBackground(color);
		if(text!=null) super.setText(text);
	}
	
	public LabelForm(Composite parent, int style,int x, int y, int width, int height) {
		super(parent, style);
		super.setBounds(x, y, width, height);
		// TODO 自动生成的构造函数存根
	}

    protected void checkSubclass() {
    }
}
