package cn.wangkai.peanut.searchchangefile.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

public class FileHeap {
	private String FilePath ="";
	private String FileOut = "";
	private String CheckDate="";
	private String OutStr="";
	private boolean isCopy=false;
	private String[] FileExts = StringUtils.split(ProUtil.read("fileexts", ".DS_Store,Thumbs.db", false), ",");
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public FileHeap() {
		
	}
	
	/**
	 * 直接拷贝
	 * @param in
	 * @param out
	 * @throws Exception
	 */
	public static void copyFileUsingFileStreams(File source, File dest) throws IOException { 
		InputStream input = null; 
		OutputStream output = null; 
		try { 
			input = new FileInputStream(source); 
			output = new FileOutputStream(dest); 
			byte[] buf = new byte[1024]; 
			int bytesRead; 
			while ((bytesRead = input.read(buf)) > 0) { 
				output.write(buf, 0, bytesRead); 
			} 
		} finally { 
			input.close(); 
			output.close(); 
		} 
	}
	
	/**
	 * FileChannel 拷贝文件
	 * @param source
	 * @param dest
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public static void copyFileUsingFileChannels(File source, File dest) throws IOException { 
		FileChannel inputChannel = null; 
		FileChannel outputChannel = null; 
		try { 
			inputChannel = new FileInputStream(source).getChannel(); 
			outputChannel = new FileOutputStream(dest).getChannel(); 
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size()); 
		} finally { 
			inputChannel.close(); 
			outputChannel.close(); 
		} 
	}
	
	/**
	 * 通过CommonIO拷贝
	 * @param source
	 * @param dest
	 * @throws IOException
	 */
	public static void copyFileUsingApacheCommonsIO(File source, File dest) throws IOException { 
		FileUtils.copyFile(source, dest); 
	}
	
	/**
	 * 使用Java7的Files类复制
	 * @param source
	 * @param dest
	 * @throws IOException
	 */
	public static void copyFileUsingJava7Files(File source, File dest) throws IOException { 
			Files.copy(source.toPath(), dest.toPath()); 
	}
	
	/**
	 * 开始处理
	 * @throws Exception
	 */
	public void DoIt() throws Exception{
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date this_date = df.parse(CheckDate);
		LinkedList<File> list = new LinkedList<File>();
		File dir = new File(FilePath);
		File file[] = dir.listFiles();
		for (int i = 0; i < file.length; i++) {
			if (file[i].isDirectory()) list.add(file[i]);
//			else System.out.println(file[i].getAbsolutePath());
		}
		File tmp;
		while (!list.isEmpty()) {
			tmp = (File) list.removeFirst();
			if (tmp.isDirectory()) {
				file = tmp.listFiles();
				if (file == null) continue;
				for (int i = 0; i < file.length; i++) {
					if (file[i].isDirectory())
						list.add(file[i]);
					else{
						Date date=new Date();
						date.setTime(file[i].lastModified());
						if(date.after(this_date)){
							String filepath = file[i].getAbsolutePath();
							String filename = file[i].getName();
							if(!checkFileExts(filename)) {
								String newpath = StringUtils.replace(filepath, FilePath, FileOut);
								if(isCopy){
									File file1 = new File(StringUtils.replace(newpath, filename, ""));
									file1.mkdirs();
									copyFileUsingApacheCommonsIO(new File(filepath), new File(newpath));
								}
								OutStr +="copy \""+filepath + "\"  \""+newpath + "\" /y\n";
							}
						}
					}
				}
//			} else {
//				System.out.println(tmp.getAbsolutePath());
			}
		}
	}

	/**
	 * 排查指定文件名文件
	 * @param filename
	 * @return
	 */
	public boolean checkFileExts(String filename) {
		for (String extfile : FileExts) {
			if(StringUtils.equalsIgnoreCase(filename, extfile)) return true;
		}
		return false;
	}
	

	public String getCheckDate() {
		return CheckDate;
	}

	public void setCheckDate(String checkDate) {
		CheckDate = checkDate;
	}

	public String getFileOut() {
		return FileOut;
	}

	public void setFileOut(String fileOut) {
		FileOut = fileOut;
	}

	public String getFilePath() {
		return FilePath;
	}

	public void setFilePath(String filePath) {
		FilePath = filePath;
	}

	public boolean isCopy() {
		return isCopy;
	}

	public void setCopy(boolean isCopy) {
		this.isCopy = isCopy;
	}

	public String getOutStr() {
		if(StringUtils.isEmpty(OutStr)){
			OutStr += "޼¼";
		}
		return OutStr;
	}

	public void setOutStr(String outStr) {
		OutStr = outStr;
	}
}
