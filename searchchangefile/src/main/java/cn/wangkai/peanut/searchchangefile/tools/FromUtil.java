package cn.wangkai.peanut.searchchangefile.tools;

import java.awt.Toolkit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class FromUtil {

	public FromUtil() {
	}

	/**
	 * 提示错误
	 * @param shell
	 * @param Message
	 */
	public static void ShowMessage(Shell shell, String Message) {
		MessageBox messageBox = new MessageBox(shell,SWT.ICON_ERROR);
		messageBox.setText("提示信息");
		messageBox.setMessage(Message);
		messageBox.open();
	}
	

	/***
	 * 窗体居中
	 * 
	 * @param shell
	 */
	public static void centerShell(Shell shell) {
		// 得到屏幕的宽度和高度
		int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
		int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
		// 得到Shell窗口的宽度和高度
		int shellHeight = shell.getBounds().height;
		int shellWidth = shell.getBounds().width;
		// 如果窗口大小超过屏幕大小，让窗口与屏幕等大
		if (shellHeight > screenHeight)
			shellHeight = screenHeight;
		if (shellWidth > screenWidth)
			shellWidth = screenWidth;
		// 让窗口在屏幕中间显示
		shell.setLocation(((screenWidth - shellWidth) / 2), ((screenHeight - shellHeight) / 2));
	}

}
