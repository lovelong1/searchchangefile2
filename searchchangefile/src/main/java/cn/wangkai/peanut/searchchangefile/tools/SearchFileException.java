package cn.wangkai.peanut.searchchangefile.tools;

public class SearchFileException extends Exception {

	private static final long serialVersionUID = 4917219890543995306L;

	public SearchFileException() {
	}
	
    public SearchFileException(String message) {
        super(message);
    }
}
